//
//  BagPackViewController.swift
//  MovieBox
//
//  Created by Cristobal Navarrete on 30/7/18.
//  Copyright © 2018 Ronny Cabrera. All rights reserved.
//

import UIKit

class BagPackViewController: UIViewController {
  var itemManager = ItemManager()
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var txtComentario: UITextField!
    
    @IBOutlet weak var cosmosVieFull: CosmosView!
    
    @IBOutlet weak var ratingSlider: UISlider!
    @IBOutlet weak var ratingLabel: UILabel!
    
    //@IBOutlet weak var txtCalificar: UITextField!
    var overview1 = ""
    var title1 = ""
    var url2 = ""
    
    private let startRating:Float = 3.7
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = title1
        overviewLabel.text = overview1
        
        cosmosVieFull.didTouchCosmos = didTouchCosmos
        cosmosVieFull.didFinishTouchingCosmos = didFinishTouchingCosmos
        ratingSlider.value = startRating //3.7 slider
        updateRating()
        
       
        
        // Do any additional setup after loading the view.
    }
    
    private func updateRating() {
        let value = Double(ratingSlider.value)
        cosmosVieFull.rating = value
        self.ratingLabel.text = BagPackViewController.formatValue(value)
        
    }
    
    @IBAction func onSliderChanget(_ sender: Any) {
    updateRating()
    }
    private class func formatValue(_ value: Double) -> String {
        return String(format: "%.2f", value)
    }
    
    private func didTouchCosmos(_ rating: Double) {
        ratingSlider.value = Float(rating)
        ratingLabel.text = BagPackViewController.formatValue(rating)
       
        ratingLabel.textColor = UIColor(red: 133/255, green: 116/255, blue: 154/255, alpha: 1)
    }
    
    private func didFinishTouchingCosmos(_ rating: Double) {
        ratingSlider.value = Float(rating)
        self.ratingLabel.text = BagPackViewController.formatValue(rating)
        ratingLabel.textColor = UIColor(red: 183/255, green: 186/255, blue: 204/255, alpha: 1)
       
    }
    
    //Base de Datos
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        
        let itemTitle = titleLabel.text ?? ""
        let itemOverview = overviewLabel.text ?? ""
        let itemDescription = txtComentario.text ?? ""
        let itemCalificar = ratingLabel.text ?? ""
        let itemImage = url2 ?? "" // path de la imagen para guardar en la base
        if itemTitle == "" {
            showAlert(title: "Error", message: "Title is required")
        }
        
        //    itemManager?.toDoItems += [item]
        
        itemManager.addItem(
            title: itemTitle,
            overview: itemOverview,
            calificar: itemCalificar,
            image: itemImage,
            itemDescription: itemDescription)
        
        navigationController?.popViewController(animated: true)
        print(itemTitle)
        print(itemOverview)
    }
    
    func showAlert(title:String, message:String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alert.addAction(okAction)
        
        present(alert, animated: true, completion: nil)
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toTable" {
            //let destination = segue.destination as! BagPackTabeViewController
            
            //let selectedRow = movieTableView.indexPathsForSelectedRows![0]
            //destination.itemInfoMovie = (movieManager, selectedRow.row)
        }
    }
    
    }
    



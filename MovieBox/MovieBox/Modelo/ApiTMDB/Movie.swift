//
//  Movie.swift
//  MovieBox
//
//  Created by Ronny Cabrera on 24/7/18.
//  Copyright © 2018 Ronny Cabrera. All rights reserved.
//

import Foundation

struct Movie: Decodable {
    let results: [MovieInfo]
}

struct MovieInfo: Decodable {
    var title: String
    var overview: String
    var poster_path: String
}

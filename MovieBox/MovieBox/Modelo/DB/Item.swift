//
//
//

import Foundation
import RealmSwift

class Item: Object {
  @objc dynamic var id:String?
  @objc dynamic var title:String?
  @objc dynamic var overview:String?
  @objc dynamic var itemDescription:String?
  @objc dynamic var calificar:String?
  @objc dynamic var image:String?
  @objc dynamic var done = false
  
  override static func primaryKey() -> String? {
    return "id"
  }
}











//
//  MovieHomeTableViewCell.swift
//  MovieBox
//
//  Created by Dario Herrea on 8/2/18.
//  Copyright © 2018 Ronny Cabrera. All rights reserved.
//

import UIKit

class MovieHomeTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var portadaPelicula: UIImageView!
    
    
    @IBOutlet weak var tituloPelicula: UILabel!
 
    @IBOutlet weak var overViewPelicula: UILabel!
    
}

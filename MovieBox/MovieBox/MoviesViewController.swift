//
//  MoviesViewController.swift
//  MovieBox
//
//  Created by Dario Herrea on 8/2/18.
//  Copyright © 2018 Ronny Cabrera. All rights reserved.
//

import UIKit
import Alamofire

class MoviesViewController: UIViewController,UITableViewDataSource, UITableViewDelegate {
    
    
    //@IBOutlet var tableView: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    
    var movieArray:[MovieInfo] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource =  self
        tableView.delegate = self
        
        conection()
        
        //tableView.reloadData()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
      
        return movieArray.count

    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "MovieCell", for: indexPath)  as! MovieHomeTableViewCell
        //let cell = UITableViewCell()
        let pelicula = movieArray[indexPath.row]
        let tituloPelicula = pelicula.title
        let overviewPelicula = pelicula.overview
        let posterPelicula = pelicula.poster_path
        
        
        cell.tituloPelicula.text = tituloPelicula
        cell.overViewPelicula.text = overviewPelicula
        
        
        let url = "https://image.tmdb.org/t/p/w342/\(posterPelicula)"
        print(url)
        Alamofire.request(url).responseImage { response in
            if let image = response.result.value {
                
                cell.portadaPelicula.image = image
            }
        }
        

        return cell
        
    }
    
    func conection(){
        
        let apiKey = "a07e22bc18f5cb106bfe4cc1f83ad8ed"
        Alamofire.request("https://api.themoviedb.org/3/movie/now_playing?api_key=\(apiKey)").responseJSON {
            response in
            
            guard let data = response.data else {
                print("error")
                return
            }
            
            guard let movie = try? JSONDecoder().decode(Movie.self, from: data)else {
                print("error decoding Movie")
                return
            }
            
            
    
                for movies in movie.results {
                    let movieAux = MovieInfo(title: movies.title, overview: movies.overview, poster_path: movies.poster_path)
                    self.movieArray.append(movieAux)
                    
                }

            //print(self.movieArray)////todo el array
            //print(movie.results[0].title)
            
            self.tableView.reloadData()
            
            
        }
        
    }
    

}




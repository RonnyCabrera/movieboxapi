//
//  SearchViewController.swift
//  MovieBox
//
//  Created by Ronny Cabrera on 24/7/18.
//  Copyright © 2018 Ronny Cabrera. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var movieTableView: UITableView!
    @IBOutlet weak var movieTextField: UITextField!
    let movieManager = MovieManager()
    var movieInf: MovieInfo!
    
    @IBAction func SearchButtonPressed(_ sender: Any) {
        let network: Network = Network()
        network.getALLMovie(of: movieTextField.text ?? "") {
            (movieArray) in
            self.movieManager.itemsMovie = movieArray
            self.movieTableView.reloadData()
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movieManager.itemsMovie.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = movieManager.itemsMovie[indexPath.row].title
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "toInfoMovieSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toInfoMovieSegue" {
            let destination = segue.destination as! InformationViewController
            let selectedRow = movieTableView.indexPathsForSelectedRows![0]
            destination.itemInfoMovie = (movieManager, selectedRow.row)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
